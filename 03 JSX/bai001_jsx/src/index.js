import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const name = "Quy Nguyen";
const fruits = [{name :'Apple'}, {name: 'Banana'}]
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    // <h1 style={{ textAlign: "center" }}>
    //     { name }
    // </h1>
    <div>
        <h1>List of fruits</h1>
        <ul>
            { fruits.map((item) => (
                <li>{ item.name }</li>
            )) }
        </ul>
    </div>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
